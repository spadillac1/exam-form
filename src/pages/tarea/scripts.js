document.addEventListener('DOMContentLoaded', () => {
  const card = document.getElementById('card');
  card.classList.remove('hidden');
  deleteClick();
});

function deleteClick(){
  const deletes = document.getElementById('delete');
  deletes.addEventListener('click', deleteCard);
}

function deleteCard(){
  const card = document.getElementById('card');
  card.classList.add('hidden');
}
