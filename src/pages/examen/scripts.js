document.addEventListener('DOMContentLoaded', () => {
  const contactForm = document.forms.namedItem('contact');
  contactForm.reset();
  loadInputEvents(contactForm);
  loadCountries();
});

document.getElementById('send').addEventListener('click', (e) => {
  e.preventDefault();
  const contactForm = document.forms.namedItem('contact');
  const name = contactForm.namedItem('name').value;
  const messagesData = {
    approved: {
      message: `Pronto nos pondremos en contacto ${name}`,
      status: 'success',
    },
    unapproved: {
      message: 'Verifica tu información',
      status: 'danger',
    },
  };
  UIkit.notification(
    messagesData[isAllInputsAproved(contactForm) ? 'approved' : 'unapproved']
  );
});

document.getElementById('range').addEventListener('change', (e) => {
  const rangeValue = document.getElementById('rangeValue');
  rangeValue.innerText = e.target.value;
})

const loadCountries = () => {
  let optionList = document.getElementById('countries').options;
  apiRequest('https://restcountries.eu/rest/v2/all',(response) =>{
    response.forEach((country) => {
      optionList.add( new Option(country.name,country.name));
    })
  });
}

const loadInputEvents = (contactForm) => {
  iterateInputs(contactForm, (input) => {
    input.addEventListener('focusout', (e) => {
      validateInput(e.target);
    });
  });
};

const validateInput = (input) => {
  const isApproved = isInputApproved(input);
  input.classList.add(isApproved ? 'uk-form-success' : 'uk-form-danger');
  input.classList.remove(isApproved ? 'uk-form-danger' : 'uk-form-success');
};

const isAllInputsAproved = (form) => {
  let status = true;
  iterateInputs(form, (input) => {
    if (!isInputApproved(input)) status = false;
  });
  return status;
};

const isInputApproved = (input) => {
  let checkbox = document.querySelector('input[name="gender"]:checked');
  let checkboxValue = checkbox ? checkbox.value : null;
  return approve.value(
    input.type !== 'radio' ? input.value.trim() : checkboxValue,
    validationRules[input.name]
  ).approved;
};

const iterateInputs = (form, callback) => {
  const inputs = [...form.getElementsByTagName('INPUT')];
  inputs.forEach((input) => {
    callback(input);
  });
};

const apiRequest = async (url, callback) => {
  const response = await fetch(url).then((r) => r.json());
  callback(response);
}
