const validationRules = {
  email: {
    email: true,
    required: true,
  },
  name: {
    format: /^[a-zA-Z]*$/,
    required: true,
  },
  lastName: {
    format: /^[a-zA-Z]*$/,
    required: true,
  },
  gender: {
    required: true,
  },
  country: {
    required: true,
  },
  street: {
    required: true,
  },
  postal: {
    numeric: true,
    min: 5,
    max: 5,
    required: true,
  },
  satisfaction: {
    min: 1,
    max: 5,
    required: true,
  },
  comment: {
    required: false,
  },
};
